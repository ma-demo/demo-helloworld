def flow
node{
  echo "bootstrap function begin"
  echo "pwd: " + pwd()
  git url: 'https://bitbucket.org/mindapproach/demo-helloworld.git'
  flow = load "flow.groovy"
  echo "bootstrap function end"
}
flow.internalBuild()
