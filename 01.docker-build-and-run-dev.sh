#!/bin/bash

echo "============================================================================================="
echo "=== build jar file"
echo "============================================================================================="
mvn clean package

echo "============================================================================================="
echo "=== run newly created jar file in docker container without creating docker image before"
echo "============================================================================================="
docker run --rm -it \
       -v $(pwd)/target/demo-helloworld-1.0-Local.jar:/opt/demo-helloworld/demo-helloworld.jar \
       -w /opt/demo-helloworld \
       java:8 \
       /usr/bin/java -jar ./demo-helloworld.jar
